# 
# Rules for compiling and linking the typechecker/evaluator
#
# Type
#   make         to rebuild the executable file langinterpreter
#   make clean   to remove all intermediate and temporary files
#   make depend  to rebuild the intermodule dependency graph that is used
#                  by make to determine which order to schedule 
#	           compilations.  You should not need to do this unless
#                  you add new modules or new dependencies between 
#                  existing modules.  (The graph is stored in the file
#                  .depend)

# These are the object files needed to rebuild the main executable file
#
OBJS = setUtils.cmo  regex.cmo syntax.cmo langparser.cmo langlexer.cmo main.cmo 

# Files that need to be generated from other files
DEPEND += langlexer.ml langparser.ml 

# When "make" is invoked with no arguments, we build an executable 
# typechecker, after building everything that it depends on
all: $(DEPEND) $(OBJS) mysplinterpreter

# Include an automatically generated list of dependencies between source files
include .depend

# Build an executable typechecker
mysplinterpreter: $(OBJS) main.cmo 
	@echo Linking $@
	ocamlc -o $@ $(COMMONOBJS) $(OBJS) 

# Compile an ML module interface
%.cmi : %.mli
	ocamlc -c $<

# Compile an ML module implementation
%.cmo : %.ml
	ocamlc -c $<

# Generate ML files from a parser definition file
langparser.ml langparser.mli: langparser.mly
	@rm -f langparser.ml langparser.mli
	ocamlyacc -v langparser.mly
	@chmod -w langparser.ml langparser.mli

# Generate ML files from a lexer definition file
%.ml %.mli: %.mll
	@rm -f $@
	ocamllex $<
	@chmod -w $@

# Clean up the directory
clean::
	rm -rf langlexer.ml langparser.ml langparser.mli *.o *.cmo *.cmi langparser.output \
	   langc TAGS *~ 

# Rebuild intermodule dependencies
depend:: $(DEPEND) 
	ocamldep $(INCLUDE) *.mli *.ml > .depend

# 
