/* File parser.mly */
%{
  open Regex;;
  open Syntax;;
%}
%token EOL EOF
%token <int> INT
%token <string> LITERAL
%token <string> VARIABLE_NAME
%token <Regex.regexTree> REGEX
%token SET_LIMIT_START SET_LIMIT_END
%token SET_LITERAL_START SET_LITERAL_BREAK SET_LITERAL_END
%token ASSIGNMENT_OPERATOR
%token PRINT
%token SET_UNION SET_INTERSECTION
%token CONCAT

%token <char> REGEX_QUANTISER
%token <char> REGEX_LITERAL
%token REGEX_START REGEX_END
%token REGEX_GROUP_START REGEX_GROUP_END
%token REGEX_ALTERNATION
%token REGEX_COUNT_START REGEX_COUNT_END REGEX_COUNT_BREAK
%token REGEX_CLASS_START REGEX_CLASS_END

%start main             /* the entry point */
%type <Syntax.syntaxTree> main

%start regex             /* the entry point */
%type <Regex.regexTree> regex

%start setliteral
%type <Syntax.setLiteralTree> setliteral

%%
main:
   expr EOL                { $1 }
 | expr EOL EOF            { $1 }
 | expr EOF                { $1 }
 | EOF                     { raise End_of_file }

;

expr:
   expr SET_LIMIT_START INT SET_LIMIT_END { LimitedSet($1, $3) }
 | LITERAL { StrLiteral($1) }
 | INT { IntLiteral($1) }
 | REGEX { SyntaxRegex( $1 ) }
 | VARIABLE_NAME ASSIGNMENT_OPERATOR expr { Assignment(VariableName($1), $3) }
 | VARIABLE_NAME { VariableName($1) }
 | expr SET_UNION expr { SetUnion( $1, $3 ) }
 | expr SET_INTERSECTION expr { SetIntersection( $1, $3 ) }
 | expr CONCAT expr { Concat( $1, $3 ) }
 | PRINT expr { Print( $2 ) }
;

setliteral:
  setliteralaux EOF { $1 }
;

setliteralaux:
  setliteral EOL { $1 }
 | SET_LITERAL_START setliteral { $2 }
 | LITERAL SET_LITERAL_BREAK setliteral { SetLiteralNode(SetLiteralLeaf($1), $3) }
 | LITERAL SET_LITERAL_END { SetLiteralLeaf($1) }
 | SET_LITERAL_START SET_LITERAL_END { EmptySet }
;

regex:
   REGEX_START regex REGEX_END EOF { $2 }
 | REGEX_COUNT_START INT REGEX_COUNT_BREAK INT REGEX_COUNT_END { RegexCount( $2, $4 ) }
 | REGEX_GROUP_START regex REGEX_GROUP_END { RegexPossibility(RegexGroup( $2 )) }
 | regex REGEX_ALTERNATION regex { RegexAlternation( $1, $3 ) }
 | REGEX_CLASS_START regex REGEX_CLASS_END { RegexPossibility(RegexCharacterClass( $2 )) }
 | REGEX_LITERAL { RegexPossibility(RegexChar( $1 )) }
 | REGEX_QUANTISER { RegexQuantiser( $1 ) }
 | regex regex { RegexTree( $1, $2 ) }
;
