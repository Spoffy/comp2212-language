(* regexTree should be left-associative *)
type regexTree = RegexTree of regexTree * regexTree 
               | RegexQuantiser of char 
               | RegexCount of int * int
               | RegexAlternation of regexTree * regexTree
               | RegexPossibility of regexPossibility

and regexPossibility = 
    RegexCharacterClass of regexTree
  | RegexChar of char
  | RegexGroup of regexTree ;;

val addIndent : string -> int -> string
val printRegex : regexTree -> int -> unit
val setFromRegexAux : regexTree -> int -> Set.Make(String).t
val setFromQuantiser : regexTree -> char -> int -> Set.Make(String).t
val generateSetFromRegex : regexTree -> int -> Set.Make(String).t
val interpretRegexTree : regexTree -> unit
