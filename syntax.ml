open Hashtbl;;
open Regex;;
open SetUtils;;

module StrSet = Set.Make(String);;

exception Unbound of string;;

type variableName = string;;
type strLiteral = string;;
type setLiteral = StrSet.t;;

type syntaxTree =
    LimitedSet of syntaxTree * int
  | StrLiteral of strLiteral
  | IntLiteral of int
  | VariableName of variableName
  | Assignment of syntaxTree * syntaxTree
  | Print of syntaxTree
  | SetUnion of syntaxTree * syntaxTree
  | SetIntersection of syntaxTree * syntaxTree
  | Concat of syntaxTree * syntaxTree
  | SyntaxRegex of Regex.regexTree;;

let wordFilter = function 
  | ":" -> ""
  | x -> x;;

type setLiteralTree =
    SetLiteralNode of setLiteralTree * setLiteralTree
  | SetLiteralLeaf of strLiteral
  | EmptySet;;

let rec setFromLiteralTree tree = match tree with
  | SetLiteralNode(leftTree, rightTree) -> StrSet.union (setFromLiteralTree leftTree) (setFromLiteralTree rightTree)
  | SetLiteralLeaf(literal) -> StrSet.singleton (wordFilter literal)
  | EmptySet -> StrSet.empty

let rec printSyntaxTree (tree: syntaxTree) (depth: int) : unit = match tree with
  | LimitedSet(tree, limit) -> print_endline (addIndent ("Limited Set " ^ (string_of_int limit)) depth); printSyntaxTree tree (depth+1)
  | StrLiteral(literal) -> print_endline (addIndent literal depth)
  | IntLiteral(literal) -> print_endline (addIndent (string_of_int literal) depth)
  | VariableName(name) -> print_endline (addIndent name depth)
  | Assignment(leftTree, rightTree) -> print_endline (addIndent "Assignment" depth); printSyntaxTree leftTree (depth+1); printSyntaxTree rightTree (depth+1)
  | Print(tree) -> print_endline (addIndent "Printing" depth); printSyntaxTree tree (depth+1)
  | SetUnion(leftTree, rightTree) -> print_endline (addIndent "Union" depth); printSyntaxTree leftTree (depth+1); printSyntaxTree rightTree (depth+1)
  | SetIntersection(leftTree, rightTree) -> print_endline (addIndent "Intersection" depth); printSyntaxTree leftTree (depth+1); printSyntaxTree rightTree (depth+1)
  | Concat(leftTree, rightTree) -> print_endline (addIndent "Concat" depth); printSyntaxTree leftTree (depth+1); printSyntaxTree rightTree (depth+1)
  | SyntaxRegex(tree) -> interpretRegexTree tree;;

let environment = Hashtbl.create 50;;

let assign key (value: StrSet.t) =
  Hashtbl.add environment key value;;

let output_size = ref 0;;
let set_output_size value = output_size := value;;

let rec interpretSyntaxTree tree = match tree with
  | Assignment(leftTree, rightTree) -> assign (interpretVariableName leftTree) (interpretAsSet rightTree)
  | Print(tree) -> printSet (setLimit (interpretAsSet tree) !output_size)
  | _ -> raise (Failure "Invalid statement at top level. Must be either an assignment or print statement.")

and interpretVariableName tree = match tree with
  | VariableName(name) -> name
  | _ -> raise (Failure "Variable name expected, got different type.")

and interpretAsSet tree = match tree with
  | LimitedSet(tree, limit) -> setLimit (interpretAsSet tree) limit
  | StrLiteral(literal) -> StrSet.singleton literal
  | VariableName(name) ->  (try Hashtbl.find environment name with Not_found -> raise (Unbound ("ERROR: Variable " ^ name ^ " unbound.")))
  | SetUnion(leftTree, rightTree) -> StrSet.union (interpretAsSet leftTree) (interpretAsSet rightTree)
  | SetIntersection(leftTree, rightTree) -> StrSet.inter (interpretAsSet leftTree) (interpretAsSet rightTree)
  | Concat(leftTree, rightTree) -> setConcat (interpretAsSet leftTree) (interpretAsSet rightTree)
  | SyntaxRegex(regexTree) -> generateSetFromRegex regexTree !output_size
  | _ -> raise (Failure "Cannot interpret value as set, failed.");;


let interpret tree = interpretSyntaxTree tree;;
