open SetUtils;;

module StrSet = Set.Make(String);;

(* regexTree should be left-associative *)
type regexTree = RegexTree of regexTree * regexTree 
               | RegexQuantiser of char 
               | RegexCount of int * int
               | RegexAlternation of regexTree * regexTree
               | RegexPossibility of regexPossibility

and regexPossibility = 
    RegexCharacterClass of regexTree
  | RegexChar of char
  | RegexGroup of regexTree ;;

let rec addIndent value amount = match amount with
  | 0 -> value
  | x -> addIndent ("-" ^ value) (amount - 1);;

let rec printRegex (tree: regexTree) (depth: int) : unit = match tree with
  | RegexAlternation(leftTree, rightTree) | RegexTree(leftTree, rightTree) ->
      print_endline (addIndent "RegexTree" depth);
      printRegex leftTree (depth+1);
      printRegex rightTree (depth+1)
  | RegexQuantiser(quantity) -> print_endline (addIndent ("Quant " ^ (Char.escaped quantity)) depth)
  | RegexCount(start, termination) -> print_endline (addIndent ((string_of_int start) ^ "," ^ (string_of_int termination)) (depth))
  | RegexPossibility(value) -> printPoss value depth

and printPoss poss depth = match poss with
  | RegexCharacterClass(tree) -> print_endline (addIndent "CharacterClass" depth) ; printRegex tree (depth+1)
  | RegexChar(literal) -> print_endline (addIndent ("Lit " ^ (Char.escaped literal)) depth)
  | RegexGroup(tree) -> print_endline (addIndent "Group" depth) ; printRegex tree (depth+1);;

(*printRegex (RegexTree(RegexPossibility(RegexChar('a')), RegexTree((RegexPossibility(RegexChar('b')), RegexQuantiser('*'))))) 0;;*)

(* Rewrite parser to only allow valid regex, e.g count, quantiser or alternation by themselves *)
(* Don't allow all regex inside class unless it's in a group! *)
let rec setFromRegexAux regexTree size = match regexTree with
  | RegexAlternation(leftTree, rightTree) ->
      StrSet.union (setFromRegexAux leftTree size) (setFromRegexAux rightTree size)
  | RegexTree(leftTree, rightTree) ->
      setFromTree (leftTree, rightTree) size
  | RegexPossibility(value) -> setFromPossibility value size
  | _ -> raise (Failure "Invalid regex: not legal at top level")

and setFromPossibility value size = match value with
  | RegexChar(character) -> StrSet.singleton (Char.escaped character)
  | RegexGroup(regex) -> setFromRegexAux regex size
  | RegexCharacterClass(classRegex) -> setFromCharacterClass classRegex size

and setFromCharacterClass regexTree size = match regexTree with
  | RegexTree(leftTree, rightTree) -> 
      StrSet.union (setFromCharacterClass leftTree size) (setFromCharacterClass rightTree size)
  | RegexPossibility(value) -> setFromPossibility value size
  | _ -> raise (Failure "Invalid regex: Illegal construct in character class, should be literal, group or class")

and setFromTree children size = match children with 
  | (leftChild, RegexQuantiser(quantiser)) ->
    setFromQuantiser leftChild quantiser size
  | (leftChild, RegexCount(startAmount, endAmount)) ->
    setFromCount leftChild startAmount endAmount size
  | (leftChild, rightChild) -> 
      setConcat (setFromRegexAux leftChild size) (setFromRegexAux rightChild size)
and setFromQuantiser regex quantiser size = match quantiser with
  | '?' -> concatXYTimes (setFromRegexAux regex size) 0 1
  | '*' -> concatXYTimes (setFromRegexAux regex size) 0 size 
  | '+' -> concatXYTimes (setFromRegexAux regex size) 1 size 
  | _ -> raise (Failure "Invalid quantiser. Seriously. Wtf. This should NEVER happen. Check the lexer.")
and setFromCount regex startAmount endAmount size =
  concatXYTimes (setFromRegexAux regex size) startAmount endAmount
;;

(* regexTree must be a tree of regex that is VALID, e.g not <?> *)
let generateSetFromRegex origRegexTree desiredSize = 
  setFromRegexAux origRegexTree desiredSize;;

let interpretRegexTree regex = StrSet.iter print_endline (generateSetFromRegex regex 6);;
