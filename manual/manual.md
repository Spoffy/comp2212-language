SPL Manual
==========

## Intro
This manual briefly describes how to use our simple SPL language, covering syntax, usage and helpful error messages.
A knowledge of GNU regular expressions will be helpful with understanding this language.

## Overview

The language is imperative, though each data type is immutable.

The language is built as a sequence of statements, with each statement delimited by a newline character. Each statement is either an output, or constructs a new set based on operations on other sets. This makes the language exceedingly fast and easy to learn and use.


## Data Types

The language has only two data types:
 * Set - A set of words ordered lexicographically
 * Word - A sequence of characters from the language's alphabet.

Both data types are immutable (cannot be modified in place).

This makes the language extremely simple to use and completely type safe.

## Syntax
### Word Literal
A word literal can be represented using "word" syntax. This creates the singleton set with word as a member, and can be used anywhere a set can be used.

### Operators

| Operator | Use | Example | Associativity |
|----------|-----|---------|---------------|
| =        | Binds the result of a statement to a variable | someVariable = value | Right |
| ..       | Concatenates two sets (every word with every other word) | set1 .. set2 | Right |
| U        | Unions two sets | set1 U set2 | Right |
| N        | Intersects two sets | set1 N set2 | Right |

### Regular Expressions

Regular expressions are used in the language to allow the programmer to easily and concisely express complex sets and ideas. They avoid the need for complicated control flow logic and instead allow for a simple, elegant expression of what a language should look like. The regular expressions used here are based on GNU extended regular expressions, so the programmer will likely be familiar with them already.

Regex replaces set literals in the language, as set literals (e.g {a, b, c}) can be expressed using a regular expression of the form "<a | b | c | d>" .

Regex are delimited by < and >.

They consist of a series of 'possibilities' which represent some language by themselves, followed by an optional modifier. There is also the '|' alternation operator.

#### Possibilities

| Regex | Meaning |
|-------|---------|
| <a>   | Language consisting of that single character |
| <[abc]> | Language consisting of a or b or c by themselves. Each can be any possibility. Synonymous to 'a &#124; b &#124; c' |
| <(abc)> | Language consisting of the language between the brackets. Any valid regex can be between the brackets. |

#### Modifiers

| Modifier | Meaning | Example |
|----------|---------|---------|
| ? | Optional. Possibility occurs 0 or 1 times. | <(abc)?> means {:, abc} |
| * | 0 or more times. Repeats infinitely. | <(abc)\*> means {:, abc, abcabc,...} |
| + | 1 or more times. Repeats infinitely. | <(abc)\+> means {abc, abcabc, ...} |
| {x, y} | From x times to y times, inclusive. | <(abc){1,2}> means {abc, abcabcd} |

### Input

Input is done by using a sequence of named variables. The first input language will be input0, the second input1, etc, with the N'th language being inputN-1.

### Output

Output is performed using the 'print' statement built in to the language. It will be automatically formatted lexicographically and output at the size specified by the input.

## Example

```
set2 = <cd(ef)*> (This is the language {cd, cdef, cdefef, ...})
set1 = set2 .. <a|b> (This is the above language, concatenated with {a, b})
print set1 (Gives {cda, cdb, cdefa, cdefb, ...})
```

## Additional Features

### Type Safety
Since the language only consists of sets and words, it is perfectly type safe. A word cannot be a set, nor vice versa.

### Error Messages

 * "Invalid Regex: Illegal construct in character class, should be literal, group or class" - A character class regex is invalid.
 * "Invalid Regex: Not legal at top level" - The regex is malformed. The parser has encountered a part of the regex that should be a complete regex by itself, but isn't.
 * "Invalid statement at top level. Must be an assignment or print statement." - There is a malformed statement, that is neither a print nor an assignment.
 * "Variable name expected, got different type" - The interpreter found a set or word when it was expecting a variable name.
 * "ERROR: Variable [name] unbound" - Attempting to use an unbound variable name.
 * "Cannot Interpret value as set. Failed." - A set was expected as part of a statement, but the part of a statement doesn't evaluate to a set.

