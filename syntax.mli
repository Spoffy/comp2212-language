type variableName = string;;
type strLiteral = string;;
type setLiteral = Set.Make(String).t

type syntaxTree =
    LimitedSet of syntaxTree * int
  | StrLiteral of strLiteral
  | IntLiteral of int
  | VariableName of variableName
  | Assignment of syntaxTree * syntaxTree
  | Print of syntaxTree
  | SetUnion of syntaxTree * syntaxTree
  | SetIntersection of syntaxTree * syntaxTree
  | Concat of syntaxTree * syntaxTree
  | SyntaxRegex of Regex.regexTree;;


type setLiteralTree =
    SetLiteralNode of setLiteralTree * setLiteralTree
  | SetLiteralLeaf of strLiteral
  | EmptySet;;

val setFromLiteralTree : setLiteralTree -> setLiteral;;
val printSyntaxTree : syntaxTree -> int -> unit;;
val interpret : syntaxTree -> unit;;
val set_output_size : int -> unit;;
val assign : string -> Set.Make(String).t -> unit;;

exception Unbound of string;;
