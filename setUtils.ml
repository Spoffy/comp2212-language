module StrSet = Set.Make(String);;

(* Concatenates all elements of set1 with all elements of set 2*)
let setConcat set1 set2 = 
  StrSet.fold (
    fun currentValue2 concatSetSoFar ->
      StrSet.union concatSetSoFar (StrSet.fold (
        fun currentValue1 subsetSoFar ->
          StrSet.add (currentValue1 ^ currentValue2) subsetSoFar
      ) set1 StrSet.empty)
  ) set2 StrSet.empty;;

(* Performs repeat regex ({x,y}, e.g b{1,3} = b, bb, bbb) but on sets *)
let concatXYTimes set x y =
  let rec repeatAux newSet count = 
    if count >= y then
      newSet
    else if count < x then
      repeatAux (setConcat newSet set) (count+1)
    else
      StrSet.union newSet (repeatAux (setConcat newSet set) (count+1))
  in
  if x == 0 then
    StrSet.add "" (repeatAux (StrSet.singleton "") 0)
  else 
    repeatAux (StrSet.singleton "") 0

let printSet set = 
  print_string "{";
  let contents = StrSet.fold (
    fun currentVal builtString ->
      builtString ^ currentVal ^ ", "
  ) set "" in
  let truncated_contents = match contents with
    | "" -> ""
    | _ -> String.sub contents 0 ((String.length contents)-2); (* -2 trims extra comma and space *)
  in
  print_string truncated_contents;
  print_string "}";
  print_newline ();;

let rec listLimit nonlimitedList limit = match (nonlimitedList, limit) with
  | ([], _) -> StrSet.empty
  | (hd :: [], _)
  | (hd :: _, 1) -> StrSet.singleton hd
  | (hd :: tl, _) -> StrSet.add hd (listLimit tl (limit-1));;

let setLimit set limit = listLimit (StrSet.elements set) limit;;

