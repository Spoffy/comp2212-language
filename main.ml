open Syntax;;

let parse_stdin () =
  let found_int = ref false in
  let count = ref 0 in
  while not !found_int do
      let line = read_line () in
      try
        set_output_size (int_of_string line);
        found_int := true
      with e ->
        (try
          assign ("input" ^ (string_of_int !count)) (setFromLiteralTree (Langparser.setliteral Langlexer.setliteral (Lexing.from_string line)));
        with 
          Parsing.Parse_error -> print_endline "ERROR: Input language incorrectly formatted.");
        count := !count + 1
  done;;

let _ = 
  parse_stdin ();
  try
    let code_input_channel = open_in (Sys.argv.(1)) in
    while true do
      let line = input_line code_input_channel in
      try
        let lexbuf = Lexing.from_string line  
        in  
           let result = Langparser.main Langlexer.language lexbuf 
           in
             interpret result; flush stdout 
      with 
         Parsing.Parse_error -> print_endline "Parser error: One or more statements in your language is likely malformed."
         | End_of_file -> ()
         | Syntax.Unbound(message) -> print_endline message
    done
  with End_of_file -> () ;;
