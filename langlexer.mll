(* File lexer.mll *)
{
open Langparser        
exception Eof
}
let regexChars = [' ' '\t' '\n' '0'-'9' '?' '+' '*' 'a'-'z' 'A'-'Z' '(' ')' '|' '{' '}' ',' '[' ']']

rule language = parse
      [' ' '\t'] {language lexbuf}
    | ['\n' ] { EOL }
    | eof { EOF }
    | ['0'-'9']+ as intString { INT(int_of_string intString) }
    | '"' ( ['a'-'z' 'A'-'Z']+ as literalString) '"' { LITERAL(literalString) }
    | 'U' { SET_UNION }
    | 'N' { SET_INTERSECTION }
    | "print" { PRINT }
    | ['a'-'z' 'A'-'Z']['a'-'z' 'A'-'Z' '0'-'9']+ as variableName { VARIABLE_NAME(variableName) }
    | '<' regexChars+ '>' as matchedRegex { REGEX(Langparser.regex regex (Lexing.from_string matchedRegex) )}
    | '[' { SET_LIMIT_START }
    | ']' { SET_LIMIT_END }
    | '=' { ASSIGNMENT_OPERATOR }
    | ".." { CONCAT }

and setliteral = parse
      [' ' '\t'] {setliteral lexbuf}
    | [ '\n' ] { EOL }
    | eof { EOF }
    | '{' { SET_LITERAL_START }
    | '}' { SET_LITERAL_END }
    | ',' { SET_LITERAL_BREAK }
    | [^ '{''}'',''\n'' ''\t']* as word { LITERAL(word) }

and regex = shortest
      [' ' '\t'] {regex lexbuf}
    | eof { EOF }
    | ['0'-'9']+ as intString { INT(int_of_string intString) }
    | ['?''+''*'] as quantiser { REGEX_QUANTISER(quantiser) }
    | ['a'-'z' 'A'-'Z'] as literal { REGEX_LITERAL(literal) }
    | '(' { REGEX_GROUP_START }
    | ')' { REGEX_GROUP_END }
    | '|' { REGEX_ALTERNATION }
    | '{' { REGEX_COUNT_START }
    | '}' { REGEX_COUNT_END }
    | ',' { REGEX_COUNT_BREAK }
    | '[' { REGEX_CLASS_START }
    | ']' { REGEX_CLASS_END }
    | '<' { REGEX_START }
    | '>' { REGEX_END }
